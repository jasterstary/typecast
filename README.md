# Typecast

Pascal class supporting arrays / collections with typecasting, typically JSON.

As I am working for years with PHP and JS, which are languages with weak typing,
I feel that strong typing in Pascal as indeed limiting.
Also the missing native way to do associative arrays.
And that Pascal approach to strings!
12 or more various schemes for storing strings, of course all incompatible with each other.
As I like Pascal, but wanted to get rid of theese quirks in my projects,
I did this class for universal storage and juggling with values.
Pascal has type "variant", but, try to use it as array.

This types could be stored in TYPECAST:

NULL, BOOL, INT, FLOAT, STRING, PCHAR, POINTER, DATE, TIME, DATETIME, ARRAY (associated or not), BLOB (array of bytes)

## Examples:

Writing associative array values, then reading them in dump and JSON:

```
var val:TTypecast;
begin
  val:=TTypecast.Create();
  val['koko']._str_:='YOU!';
  val['loko']._str_:='ME!';
  val['roko']['alpha']._int_:=1;
  val['roko']['beta']._str_:='beta';
  val['roko']['koko']._str_:='koko???';
  val['roko']['alpha']._int_:=5;
  val.dump();
  writeln(val._json_);
  writeln('COUNT:', val.count());
  val.Free();
end;
```

Reading JSON into associative array, then reading one of values deeper in tree:
```
var val:TTypecast;
begin
  val:=TTypecast.Create();
  val._json_:='{"name":"Andy", "age":50, "children": [{"name":"Adam", "age":7},{"name":"Alex", "age":19}]}';
  writeln(val['children'][0]['name']._str_);
  writeln(val['children'][1]['name']._str_);
  val.Free();
end;
```

Iteration through associative array:
```
procedure listing(value:TTypeCast);
var row:TTypeCast=nil;
begin
  value.reset(row);
  while value.each(row) do
  begin
    writeln('key: ', row._key_, ', value: ', row._str_);
  end;
end;

```

## Properties:

#### BOOL
```
ttypecast._bool_
```
Writing this property will set boolean as main type of instance.
Reading property returns main value of instance (regardless actual type), casted to boolean.
Property is accessible also with:
```
procedure ttypecast._bool(Value: Boolean); function ttypecast.bool_(): Boolean;
```

---

#### INT
```
ttypecast._int_
```
Writing this property will set integer as main type of instance.
Reading property returns main value of instance (regardless actual type), casted to integer.
Property is accessible also with:
```
procedure ttypecast._int(Value: Integer);  function ttypecast.int_(): Integer;
```

---

#### FLOAT
```
ttypecast._float_
```
Writing this property will set real number as main type of instance.
Reading property returns main value of instance (regardless actual type), casted to float.
Property is accessible also with:
```
procedure ttypecast._float(Value: Float); function ttypecast.float_(): Float;
```

---

#### STR
```
ttypecast._str_
```
Writing this property will set ansistring as main type of instance.
Reading property returns main value of instance (regardless actual type), casted to string.
Property is accessible also with:
```
procedure ttypecast._str(Value: AnsiString); function ttypecast.str_(): AnsiString;
```

---

#### DATE
```
ttypecast._date_
```
Writing this property will set date as main type of instance.
Reading property returns main value of instance (regardless actual type), casted to date (output is string).
Property is accessible also with:
```
procedure ttypecast._date(Value: AnsiString); function ttypecast.date_(): AnsiString;
```

---

#### TIME
```
ttypecast._time_
```
Writing this property will set time as main type of instance.
Reading property returns main value of instance (regardless actual type), casted to time (output is string).
Property is accessible also with:
```
procedure ttypecast._time(Value: AnsiString); function ttypecast.time_(): AnsiString;
```

---

#### DTIME
```
ttypecast._dtime_
```
Writing this property will set dtime as main type of instance.
Reading property returns main value of instance (regardless actual type), casted to dtime (output is string).
Property is accessible also with:
```
procedure ttypecast._dtime(Value: AnsiString); function ttypecast.dtime_(): AnsiString;
```

---

#### DT
```
ttypecast._dt_
```
Writing this property will set dt as main type of instance.
Reading property returns main value of instance (regardless actual type), casted to dt (output is TDateTime).
Property is accessible also with:
```
procedure ttypecast._dt(Value: TDateTime); function ttypecast.dt_(): TDateTime;
```

---


#### PCHAR
```
ttypecast._pchar_
```
Writing this property will set pchar as main type of instance.
Reading property returns main value of instance (regardless actual type), casted to pchar.
Property is accessible also with:
```
procedure ttypecast._pchar(Value: Pchar); function ttypecast.pchar_(): Pchar;
```

---

#### PTR
```
ttypecast._ptr_
```
Writing this property will set pointer as main type of instance.
Reading property returns either pointer to main value of instance, or pointer, if actual type is indeed pointer.
Property is accessible also with:
```
procedure ttypecast._ptr(Value: Pointer); function ttypecast.ptr_(): Pointer;
```

---

#### ARRAY
```
ttypecast[key]
```
Writing this property will set associative array as main type of instance,
and assigns new instance of typecast on position with key "Index".
Reading property returns either existing instance of typecast,
if actual type is array, and index was already set,
or, if that index was not already set, new instance of typecast is returned.
If in the next step this new and empty instance is assigned to value, that index become set.
Property is accessible also with:
```
procedure ttypecast._fld (Index: Variant; Value: TTypecast); function ttypecast.fld_ (Index: Variant): TTypecast;
```
---

#### BLOB

Blob has no property, as Pascal doesnt provide nothing you could assign directly.
It is accessible only through methods:

Write:
```
function ttypecast.blockwrite(buffer:pointer;size:longint;blocksize:integer):boolean;
```
Read:
```
function ttypecast.blockread(buffer:pointer;size:longint;blocksize:integer):boolean;
```

---

#### JSON
```
ttypecast._json_
```
Writing this property with valid JSON string will set associative array as main type of instance,
and the whole tree of arrays and values is build upon current instance of typecast.
Reading this property will return valid JSON string, representing the whole tree, associated with current instance of typecast.
Property is accessible also with:
```
procedure ttypecast._json(s: ansistring); function ttypecast.json_(): ansistring;
```

---

#### SIZE
```
ttypecast._size_
```
Reading this property returns size of storage space in bytes.
Property is read only.
```
function ttypecast.size_(): longint;
```

---

#### TYPE
```
ttypecast._type_
```
Reading this property returns actual main type of instance.
Writing this property causes that actual main type of instance will be changed to another type.
Available types are:

[ bool | int | float | str | pchar | ptr | array | blob ]

Property is accessible also with:
```
procedure ttypecast._type(Value: string); function ttypecast.type_(): string;
```

---

#### IDX
```
ttypecast._idx_
```
Reading this property will return array position of instance inside parent instance of typecast.
If current instance is not part of array, zero is returned.
Property is read-only and is accessible also with:
```
procedure ttypecast._idx(Value: Longint); function ttypecast.idx_(): Longint;
```

---

#### KEY
```
ttypecast._key_
ttypecast._keystr_
```
Reading this property will return array key of instance inside parent instance of typecast.
If current instance is not part of array, zero is returned.
For keystr property: this will return string representation of key, or empty string.

```
procedure ttypecast._key(Value: Variant); function ttypecast.key_(): Variant; function keystr_(): string;
```

---
#### OWNER

```
ttypecast._owner_
```
```
function owner_(): Variant;
```

## testing functions:
```
function ttypecast.isSet():boolean;
```
Returns true, if value was already assigned.
```
function ttypecast.isEmpty():boolean;
```
Returns false, if value was already assigned.
```
function ttypecast.isNull():boolean;
```
Returns true, if value was assigned as null.

```
function ttypecast.isIndexedArray():boolean;
```
Returns true, if instance is indexed array (not associative).
In other words, all array keys are numeric.
```
function ttypecast.isArray():boolean;
```
Returns true, if instance is array (associative or not).
```
function ttypecast.isString():boolean;
```
Returns true, if instance value is actually string.
```
function ttypecast.isBoolean():boolean;
```
Returns true, if instance value is actually boolean.
```
function ttypecast.isInteger():boolean;
```
Returns true, if instance value is actually integer.

```
function ttypecast.isReal():boolean;
```
Returns true, if instance value is actually real.
```
function ttypecast.isPchar():boolean;
```
Returns true, if instance value is actually pchar.
```
function ttypecast.isPointer():boolean;
```
Returns true, if instance value is actually pointer.
```
function ttypecast.isBlob():boolean;
```
Returns true, if instance value is actually blob.
```
function ttypecast.isChars():boolean;
```
Returns true, if instance value is actually blob, where all bytes are chars.
```
function ttypecast.isZeroTerminated():boolean;
```
Returns true, if instance value is actually blob, where all bytes are chars, except the last one byte, which is zero.
Such blob is compatible with string.

## Blob functions:
```
function ttypecast.blockwrite(buffer:pointer;size:longint;blocksize:integer):boolean;
```

```
function ttypecast.blockread(buffer:pointer;size:longint;blocksize:integer):boolean;
```

## Conversion methods:

```
function ttypecast.zeroTerminate():boolean;
function ttypecast.convertToString():boolean;
function ttypecast.expandJSON():boolean;
function ttypecast.explode(delimiter:string):boolean;
function ttypecast.toJSON():ansistring;
function ttypecast.fromJSON(json:ansistring): boolean;
function ttypecast.fromJSONpchar(json:pchar): boolean;
```

## Dump methods:

```
function ttypecast.printhex():boolean;
function ttypecast.print():boolean;
procedure ttypecast.dump();
function ttypecast._dumpType(Index: Variant): Integer;
```

## NULL methods:
```
procedure ttypecast.null();
```
Type is changed to null.

## DTime methods:
```
function ttypecast.now(): ansistring;
```
Type is changed to dtime and set with current datetime.
Returns string representation of value.


## String methods:
```
function ttypecast.explode(delimiter:string):boolean;
```
If type is string, then string is split by delimiter and type is changed to array.
Returns success as boolean.

## Array methods:
```
function ttypecast.array_keys():TTypecast;
```
Returns new instance of typecast, which is filled with indexed array of keys from current instance.

```
function ttypecast.array_values():TTypecast;
```
Returns instance of typecast, which is filled with indexed array of values from current instance.

```
function ttypecast.copy(Value: TTypecast): TTypecast;
```


```
function ttypecast.merge(Value: TTypecast): TTypecast;
```

```
function ttypecast.unset(Index: Variant): TTypecast;
```
Field with key or index "Index" in current instance of typecast is emptied, set to neutral state.
Subsequent call to ttypecast[Index].isSet() will return false.


```
procedure ttypecast.clear();
```
Current instance of typecast is emptied, set to neutral state.
Subsequent call to ttypecast.isSet() will return false.

```
function ttypecast.count(): Integer;
```
Returns count of array items in current instance of typecast.


### Iteration methods:
For iteration we will need one extra variable of type TTypeCast.
In each iteration, this variable is copy of currently iterated TTypeCast.
It is important, to assign nil to row variable, before reset.
Example:

```
procedure listing(value:TTypeCast);
var row:TTypeCast=nil;
begin
  value.reset(row);
  while value.each(row) do
  begin
    writeln('key: ', row._key_, ', value: ', row._str_);
  end;
end;
```


### Helper functions:
There are available some additional helper functions utilizing TTypeCast.


```
function typecast_regexpr(s,regexp: string):TTypeCast;
```
Taking string s, search for regexp, returns typecast with found matches as keys.


```
function typecast_find(key: string; tt:TTypeCast):TTypecast;
```
Taking string key, search for key in TT typecast, returns either found typecast or nil.

```
function typecast_format(s: string; vars:TTypeCast):string;
```
Taking string s, returns string with all occurences of keys replaced with values from vars typecast.

example:
```

String "{{some.key}}" will be replaced with vars['some']['key'] value.

```


### Deprecated:
This, older way of iteration is now deprecated:

```
// DEPRECATED!
var row:TIter;
begin
  val.ForeachStart(row);
  while val.Foreach(row) do
  begin
    writeln(row.idx, ' ', row.key, ' ', TTypecast(row.value)._str_);
  end;
  val1.ForeachEnd(row);
end;
```


```
function typecast.Foreach(var row: TIter): Boolean;
procedure typecast.ForeachStart(var row: TIter);
procedure typecast.ForeachEnd(var row: TIter);
```

## License:
Typecast is licensed by MIT License:

```
Copyright (c) 2018 Jaster Stary

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```



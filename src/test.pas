program test;
uses sysutils, Typecast;
var i : byte;

procedure listing(value:TTypeCast);
var row:TTypeCast=nil;
begin
  writeln('listing');
  value.reset(row);
  while value.each(row) do
  begin
    writeln(' key: ', row._key_, ' value: ', row._str_);
  end;
  writeln('listing done');
end;

procedure persons();
var val:TTypecast;
begin
  val:=TTypecast.Create();
  val._json_:='{"name":"Andy", "age":50, "car":null, "children": [{"name":"Adam", "age":7},{"name":"Alex", "age":19}]}';
  writeln(val['children'][0]['name']._str_);
  writeln(val['children'][1]['name']._str_);
  val.dump();
  writeln(val._json_);
  val.Free();
end;

procedure a();
var
  val:TTypecast;
  cp:TTypecast=nil;
  dp:TTypecast=nil;
  ff,xx:TTypecast;
  p:pchar;
  pp:pointer;
  i:integer;

begin
  val:=TTypecast.Create();
  val._json_:='{"widget":{"debug":"on", "window": { "title": "Sample Konfabulator Widget", "name": "main_window", "width": 500, "height": 500 }, "image": { "src": "Images/Sun.png", "name": "sun1", "hOffset": 250, "vOffset": 250, "alignment": "center"    },    "text": {        "data": "Click Here",        "size": 36,        "style": "bold",        "name": "text1",        "hOffset": 250,        "vOffset": 100,        "alignment": "center",        "onMouseUp": "sun1.opacity = (sun1.opacity / 100) * 90;" }}}';
  val['okey']._str_:='ha!';
  val['ara']['gogo']._str_:='ara';
  val['ara']['gege']._str_:='araw';
  val['arab']['gogo']._str_:='ara1';
  val['arak']['gogo']._str_:='ara65';
  val['arak']['gigi']._str_:='ara65';
  val['araf']['gogo']._str_:='araščšč';
  val['arafat']['gogo']['gogo']._str_:='araščšč';
  val['arafat']['gogo']['gogol']._str_:='araščšč';
  val['arafat']['gogo']['gumbo']._str_:='araščšč';

  val['arafat']['gogor']['gogo']._str_:='araščšč 1';
  val['arafat']['gogor']['gogol']._str_:='araščšč 2';
  val['arafat']['gogor']['gumbo']._str_:='araščšč 3';

  val['arat']['gogo']._str_:='aradsd';
  val['array']._str_:='ha!';

  val['datetime'].now();

  listing(val['arafat']['gogor']);
  //listing(val);
{
  i:=0;
  while val.array_shift(cp) do
  begin
    writeln('........', val.count());
    cp.dump();

    if i<20 then
    begin
      dp:=TTypeCast.Create();
      dp['a']._str_:='THIS is THE LAST ROW, '+inttostr(i)+' !';
      dp['b']._str_:='ho ho ho!!!';

      val.array_push(dp);

      dp.free();
    end;
    inc(i);
    //sleep(500);
  end;
}
  //cp:=TTypecast.Create();
  {
  val.reset(cp);
  while val.each(cp) do
  begin
    //writeln(cp._str_);
    writeln('........');
    cp.dump();
    val.unset(cp._key_);
    val.reset(cp);
  end;

  }
  //cp.free();
  writeln('===============');
  val.dump();
  writeln(val.toJSON());
  //val.free();
  //exit;
  //val.setSetup(32);
  val._int(12);
  writeln(val.int_());
  val._str_:='kuna';
  writeln(val._str_);
  p:=pchar('líška');
  val._pchar_:=p;
  writeln(p);
  writeln(val._pchar_);
  val._str_:='kuna';
  writeln(val._str_);
  writeln(val._bool_);
  val._type_:='array';
  val.dump();
  val._bool_:=true;
  writeln(val._bool_);
  val.clear();
  writeln(val._bool_);
  val._key_:='BASE';
  val['koko']._str_:='YOU!';
  val['loko']._str_:='ME!';
  val['roko']['alpha']._str_:='alpha';
  val['roko']['beta']._str_:='beta';
  val['roko']['koko']._str_:='koko???';
  val['roko']['alpha']._str_:='yo!';
  val['koko']._str_:='GOO!';
  pp:=getmem(3);
  fillChar(pp^, 3, 'Q');
  val['kuk'].blockwrite(pp,3,0);
  freemem(pp, 3);
  writeln(val['kuk'].isZeroTerminated());
  val['kuk']._type_:='str';
  val['json']._str_:='{"toto":"je json","a toto hlava":22}';
  val['json'].expandJSON();
  val.dump();
  writeln(val.toJSON());

  val.clear();
  writeln('CLEAR, COUNT:', val.count());

  writeln('---------------------------');
  val._json_:='{"xx":false, "aa":"goo", "bb":55, "ara":["a","b","c"], "cc":{"aa":"goob", "bb":35, "boa":{"a":1,"b":1}, "dd":false}}';
  writeln('---------------------------');
  writeln('COUNT:', val.count());
  val.dump();
  writeln(val.toJSON());
  cp:=TTypecast.Create();
  cp.copy(val);
  cp.merge(val['cc']);
  cp['boa'].unset('a');
  cp['xx'].copy(val['aa']);
  writeln('Going free...');
  val.Free();
  cp['bb']._int_:=cp['bb']._int_+1;

  cp.dump();
  writeln(cp.toJSON());

  ff:=typecast_find('cc.boa.a', cp);
  if ff<>nil then
  begin
    writeln('FF found:', ff._str_);
    ff.dump();
  end else begin
    writeln('FF not found.');
  end;
  cp['datetime'].now();
  writeln(typecast_format('Now {{datetime}}, {{cc.boa.a}} was {{not.found}} or {{xx}}!', cp));
  writeln('free');
  cp.Free();
  persons();
end;

BEGIN
  a();
END.

